package com.sisdam.loyal.auditlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuditLibApplication.class, args);
	}

}
