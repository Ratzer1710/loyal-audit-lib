package com.sisdam.loyal.auditlib.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

public class AuditDto {
    public enum ActionType {
        GET, POST, PUT, DELETE
    }

    private OffsetDateTime timestamp;
    private String username;
    private ActionType actionType;
    private String actionOrigin;
    private String action;

    public AuditDto(OffsetDateTime timestamp, String username, ActionType actionType, String actionOrigin, String action) {
        this.timestamp = timestamp;
        this.username = username;
        this.actionType = actionType;
        this.actionOrigin = actionOrigin;
        this.action = action;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getActionOrigin() {
        return actionOrigin;
    }

    public void setActionOrigin(String actionOrigin) {
        this.actionOrigin = actionOrigin;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AuditDto)) {
            return false;
        }
        AuditDto that = (AuditDto) obj;
        return Objects.equals(this.timestamp, that.timestamp)
                && Objects.equals(this.username, that.username)
                && Objects.equals(this.actionType, that.actionType)
                && Objects.equals(this.actionOrigin, that.actionOrigin)
                && Objects.equals(this.action, that.action);
    }
}
