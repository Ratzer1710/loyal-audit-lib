package com.sisdam.loyal.auditlib.service;

import com.sisdam.loyal.auditlib.domain.AuditDto;

import java.io.IOException;
import java.net.URISyntaxException;

public interface AuditService {
    void audit(AuditDto auditDto) throws IOException;
    void auditError(AuditDto auditDto, Exception e) throws IOException;
}
