package com.sisdam.loyal.auditlib.service;

import com.sisdam.loyal.auditlib.domain.AuditDto;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class AuditServiceImpl implements AuditService{

    public void audit(AuditDto auditDto) throws IOException {
        final HttpPost httpPost = new HttpPost("http://localhost:8081/audit");
        final String json = "{\"timestamp\": \"" + auditDto.getTimestamp().toString() + "\", " +
                "\"username\": \"" + auditDto.getUsername() + "\", " +
                "\"actionType\": \"" + auditDto.getActionType().name() + "\", " +
                "\"actionOrigin\": \"" + auditDto.getActionOrigin() + "\", " +
                "\"action\": \"" + auditDto.getAction() + "\"}";
        final StringEntity entity = new StringEntity(json);

        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpClient client = HttpClients.createDefault();
        client.execute(httpPost);
    }

    public void auditError(AuditDto auditDto, Exception e) throws IOException {
        final HttpPost httpPost = new HttpPost("http://localhost:8081/audit/error");
        final String json = "{\"timestamp\": \"" + auditDto.getTimestamp().toString() + "\", " +
                "\"username\": \"" + auditDto.getUsername() + "\", " +
                "\"actionType\": \"" + auditDto.getActionType().name() + "\", " +
                "\"actionOrigin\": \"" + auditDto.getActionOrigin() + "\", " +
                "\"action\": \"" + auditDto.getAction() + "\", " +
                "\"exception\": \"" + e.getClass().getSimpleName() + "\", " +
                "\"exceptionMessage\": \"" + e.getMessage() + "\"}";
        final StringEntity entity = new StringEntity(json);

        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpClient client = HttpClients.createDefault();
        client.execute(httpPost);
    }
}
